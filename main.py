# -*- coding: utf-8 -*-
from modules import *


def query_fetch_all_records(query):
    print('Query fetch all: ' +  query)
    cursor.execute(query)
    result = cursor.fetchall()
    for x in result:
        print(x)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('Analist')
    [conn, cursor] = connect_db('localhost', 'root', 'j5YybDrUm@7Q]Whk9rJ#k', 'mysql')
    #query_fetch_all_records('select * from mysql.db;')
    load_csv_into_db('impiegati.csv', 'localhost','root', 'j5YybDrUm@7Q]Whk9rJ#k', 'company', 'employees')

    # Esecuzione della query di analisi
    #query = "analyze table ac.prt_ac_report partition (annomese_tipo_prt='202308_AC2') compute statistics for columns;"
    #execute_hive_query(query)

    # Controllo delle partizioni e sottopartizioni
    #check_partitions("ac.prt_ac_report")

    # Memorizzazione delle informazioni sulla query
    #store_query_information(query)

    # Ottenere il timestamp dell'ultima query di analisi effettuata
    #last_analysis_timestamp = get_last_analysis_query_timestamp()
    #print("Timestamp dell'ultima query di analisi effettuata:", last_analysis_timestamp)

