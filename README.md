## Requirements
- Python 2.7.5 <br>
- MySQL 8.0.36 <br>

## Installation required (run as root user)
$ yum install -y python-devel mysql-devel <br>
$ pip install -r requirements <br>

## Execution
$ python main.py <br>
