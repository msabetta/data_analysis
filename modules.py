# coding=utf-8
import subprocess
#import MySQLdb
import time
import mysql.connector
import csv
import pyhive
from datetime import datetime


# Funzione per eseguire una query Hive
def execute_hive_query(query):
    command = "hive -e '{}'".format(query)
    subprocess.call(command, shell=True)

# Funzione per controllare le partizioni e le sottopartizioni
def check_partitions(database, table, hive_cursor):
    # Recupero delle partizioni
    #query_partitions = f"SHOW PARTITIONS {database}.{table}"
    query_partitions = "SHOW PARTITIONS {}.{}".format(database,table)
    hive_cursor.execute(query_partitions)
    partitions = hive_cursor.fetchall()

    if not partitions:
        print("La tabella {}.{} non ha partizioni.".format(database,table))
    else:
        print("Partizioni della tabella {}.{}:".format(database,table))
        for partition in partitions:
            print(partition[0])

            # Recupero delle sottopartizioni
            query_subpartitions = "SHOW PARTITIONS {}.{} PARTITION ({})".format(database,table,partition[0])
            hive_cursor.execute(query_subpartitions)
            subpartitions = hive_cursor.fetchall()

            if subpartitions:
                print("\tSottopartizioni:")
                for subpartition in subpartitions:
                    print("\t{}".format(subpartition[0]))


def analyze_partitions(database, table, hive_cursor):
    # Recupero delle partizioni
    query_partitions = "SHOW PARTITIONS {}.{}".format(database,table)
    hive_cursor.execute(query_partitions)
    partitions = hive_cursor.fetchall()

    if not partitions:
        print("La tabella {}.{} non ha partizioni.".format(database,table))
    else:
        print("Analisi delle partizioni della tabella {}.{}:".format(database,table))
        for partition in partitions:
            print("\nPartizione: {}".format(partition[0]))

            # Analisi statistica dettagliata per colonne
            query_analyze = "DESCRIBE EXTENDED {}.{} PARTITION ({})".format(database,table,partition[0])
            hive_cursor.execute(query_analyze)
            analysis = hive_cursor.fetchall()

            for row in analysis:
                print("\t", row[0], ":", row[1])

            # Recupero delle sottopartizioni
            query_subpartitions = "SHOW PARTITIONS {}.{} PARTITION ({})".format(database,table,partition[0])
            hive_cursor.execute(query_subpartitions)
            subpartitions = hive_cursor.fetchall()

            if subpartitions:
                print("\n\tSottopartizioni:")
                for subpartition in subpartitions:
                    print("\t{}".format(subpartition[0]))
                    # Analisi statistica dettagliata per colonne
                    query_analyze_subpartition = "DESCRIBE EXTENDED {}.{} PARTITION ({},{})".format(database,table,partition[0],subpartition[0])
                    hive_cursor.execute(query_analyze_subpartition)
                    analysis_sub = hive_cursor.fetchall()

                    for row in analysis_sub:
                        print("\t\t", row[0], ":", row[1])

def execute_hive_analyze_query(hive_cursor,table_name, partition):
    query = "ANALYZE TABLE {} PARTITION ({}) COMPUTE STATISTICS FOR COLUMNS".format(table_name,partition)
    hive_cursor.execute(query)
    return query


# Funzione per memorizzare le query e le informazioni correlate in MySQL
def store_query_information(query_executed,description_query,db_host, db_user, db_password):
    [conn, cursor] = connect_db(db_host, db_user, db_password, 'mysql')

    # Creazione del database (se non esiste)
    cursor.execute("CREATE DATABASE IF NOT EXISTS hive_analysis;")
    conn.commit()

    cursor.execute("USE hive_analysis;")
    conn.commit()

    # Creazione della tabella (se non esiste)
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS hive_analysis.queries (
        id INT AUTO_INCREMENT PRIMARY KEY,
        query_executed TEXT,
        timestamp DATETIME,
        description_query TEXT
    );
    """)
    conn.commit()
    #crea timestamp
    timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
    #insert data query
    cursor.execute("INSERT INTO hive_analysis.queries (query_executed, timestamp, description_query) VALUES (%s, %s, %s)", (query_executed, timestamp,description_query))
    conn.commit()

    conn.close()

# Funzione per ottenere il timestamp dell'ultima query di analisi effettuata
def get_last_analysis_query_timestamp(db_host, db_user, db_password):
    [conn, cursor] = connect_db(db_host, db_user, db_password, 'hive_analysis')
    cursor.execute("SELECT timestamp FROM hive_analysis.queries ORDER BY timestamp DESC LIMIT 1")
    result = cursor.fetchone()
    conn.close()
    return result[0] if result else None

def connect_db(host,user,passwd,database):
    conn = mysql.connector.connect(host=host, user=user, passwd=passwd, db=database, auth_plugin='mysql_native_password')
    cursor = conn.cursor()
    return conn, cursor

def close_connection(conn):
    return conn.close()

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print("Hi, {}".format(name))  # Press Ctrl+F8 to toggle the breakpoint.


def load_csv_into_db(csv_file, db_host, db_user, db_password, db_name, table_name):
    # Connessione al database
    [conn, cursor] = connect_db(db_host,db_user,db_password,'mysql')

    # Creazione del database
    cursor.execute("CREATE DATABASE IF NOT EXISTS {}".format(db_name))
    conn.cmd_init_db(db_name)

    # Creazione della tabella "employees"
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS {} (
            id INT,
            nome VARCHAR(50),
            cognome VARCHAR(50),
            dipartimento VARCHAR(50),
            stipendio FLOAT,
            data_assunzione VARCHAR(50)
        );
    """.format(table_name))

    try:
        # Apertura del file CSV
        with open(csv_file, 'rb') as csvfile:
            csvreader = csv.reader(csvfile)

            # Estrazione dell'intestazione del CSV
            headers = next(csvreader)

            # Creazione della tabella nel database
            create_table_query = "CREATE TABLE IF NOT EXISTS {} (".format(table_name)
            for header in headers:
                create_table_query += "{} VARCHAR(255), ".format(header.strip())
            create_table_query = create_table_query[:-2] + ")"
            cursor.execute(create_table_query)

            # Inserimento dei dati nel database
            for row in csvreader:
                placeholders = ', '.join(['%s'] * len(row))
                insert_query = "INSERT INTO {} ({}) VALUES ({})".format(table_name, ', '.join(headers), placeholders)
                cursor.execute(insert_query, row)

        # Salvataggio dei cambiamenti e chiusura della connessione
        conn.commit()
        print("File CSV caricato con successo nel database MySQL.")
    except Exception as e:
        print("Si è verificato un errore durante il caricamento del file CSV:", e)
    finally:
        cursor.close()
        conn.close()


def connect_hive(db_host, port):
    hive_conn = pyhive.hive.connect(db_host,port)
    cursor = hive_conn.cursor()
    return hive_conn, cursor